From mathcomp.ssreflect Require Import ssreflect ssrfun ssrbool eqtype.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive term : Type :=
  | TmTrue : term
  | TmFalse : term
  | TmIfThenElse : term -> term -> term -> term
  | TmZero : term
  | TmSucc : term -> term
  | TmPred : term -> term
  | TmIsZero : term -> term.

Inductive evaluate : term -> term -> Prop :=
  | EIfTrue : forall t2 t3, evaluate (TmIfThenElse TmTrue t2 t3) t2
  | EIfFalse : forall t2 t3, evaluate (TmIfThenElse TmFalse t2 t3) t3
  | EIf : forall t1 t2 t3 t4, evaluate t1 t2 -> evaluate (TmIfThenElse t1 t3 t4) (TmIfThenElse t2 t3 t4)
  | ESucc : forall t1 t2, evaluate t1 t2 -> evaluate (TmSucc t1) (TmSucc t2)
  | EPredZero : evaluate (TmPred TmZero) TmZero
  | EPredSucc : forall t, evaluate (TmPred (TmSucc t)) t
  | EPred : forall t1 t2, evaluate t1 t2 -> evaluate (TmPred t1) (TmPred t2)
  | EIsZeroZero : evaluate (TmIsZero TmZero) TmTrue
  | EIsZeroSucc : forall t, evaluate (TmIsZero (TmSucc t)) TmFalse
  | EIsZero : forall t1 t2, evaluate t1 t2 -> evaluate (TmIsZero t1) (TmIsZero t2).

Inductive type : Type :=
  | Nat : type
  | Bool : type.

Inductive has_type : term -> type -> Prop :=
  | TyTrue : has_type TmTrue Bool
  | TyFalse : has_type TmFalse Bool
  | TyIf : forall t1 t2 t3 ty, has_type t1 Bool -> has_type t2 ty -> has_type t3 ty -> has_type (TmIfThenElse t1 t2 t3) ty
  | TyZero : has_type TmZero Nat
  | TySucc : forall t, has_type t Nat -> has_type (TmSucc t) Nat
  | TyPred : forall t, has_type t Nat -> has_type (TmPred t) Nat
  | TyIsZero : forall t, has_type t Nat -> has_type (TmIsZero t) Bool.

Inductive is_number : term -> Prop :=
  | NZero : is_number TmZero
  | NSucc : forall t, is_number t -> is_number (TmSucc t).

Inductive is_value : term -> Prop :=
  | VTrue : is_value TmTrue
  | VFalse : is_value TmFalse
  | VNumber : forall t, is_number t -> is_value t.

Lemma le_8_2_2_1 : forall r, has_type TmTrue r -> r = Bool.
  elim=> H //. inversion H. 
Qed.

Lemma le_8_2_2_2 : forall r, has_type TmFalse r -> r = Bool.
  elim=> H //. inversion H. 
Qed.

Lemma le_8_2_2_3 : forall t1 t2 t3 r, has_type (TmIfThenElse t1 t2 t3) r -> has_type t1 Bool /\ has_type t2 r /\ has_type t3 r.
  move=> t1 t2 t3 r H. by inversion H.
Qed.

Lemma le_8_2_2_4 : forall r, has_type TmZero r -> r = Nat.
  elim=> H //. inversion H.
Qed.

Lemma le_8_2_2_5 : forall t r, has_type (TmSucc t) r -> r = Nat /\ has_type t Nat.
  move=> t r H. by inversion H.
Qed.

Lemma le_8_2_2_6 : forall t r, has_type (TmPred t) r -> r = Nat /\ has_type t Nat.
  move=> t r H. by inversion H.
Qed.

Lemma le_8_2_2_7 : forall t r, has_type (TmIsZero t) r -> r = Bool /\ has_type t Nat.
  move=> t r H. by inversion H.
Qed.

Lemma le_8_2_3_1 t1 t2 t3 ty : has_type (TmIfThenElse t1 t2 t3) ty -> has_type t1 Bool /\ has_type t2 ty /\ has_type t3 ty.
  move=> P. by inversion P.
Qed.

Lemma le_8_2_3_2 t : has_type (TmSucc t) Nat -> has_type t Nat.
  move=> P. by inversion P.
Qed.


Lemma le_8_2_3_3 t : has_type (TmPred t) Nat -> has_type t Nat.
  move=> P. by inversion P.
Qed.

Lemma le_8_2_3_4 t : has_type (TmIsZero t) Bool -> has_type t Nat.
  move=> P. by inversion P.
Qed.

Theorem th_8_2_4 : forall t r1 r2, has_type t r1 -> has_type t r2 -> r1 = r2.
  move=> t r1 r2. elim t => [H1 H2|H1 H2|t1 H1 t2 H2 t3 H3 H4 H5|H1 H2|t1 H1 H2 H3|t1 H1 H2 H3|t1 H1 H2 H3].
    - by rewrite (le_8_2_2_1 H1) (le_8_2_2_1 H2).
    - by rewrite (le_8_2_2_2 H1) (le_8_2_2_2 H2).
    - apply le_8_2_2_3 in H4. apply le_8_2_2_3 in H5. tauto.
    - by rewrite (le_8_2_2_4 H1) (le_8_2_2_4 H2).
    - by rewrite (proj1 (le_8_2_2_5 H2)) (proj1 (le_8_2_2_5 H3)).
    - by rewrite (proj1 (le_8_2_2_6 H2)) (proj1 (le_8_2_2_6 H3)).
    - by rewrite (proj1 (le_8_2_2_7 H2)) (proj1 (le_8_2_2_7 H3)).
Qed.

Theorem th_8_3_1_1 v : has_type v Bool -> is_value v -> v = TmTrue \/ v = TmFalse.
  move=> P Q. inversion Q.
    - by left.
    - by right.
    - inversion H.
      + rewrite -H1 in P. inversion P.
      + rewrite -H2 in P. inversion P.
Qed.

Theorem th_8_3_1_2 v : has_type v Nat -> is_value v -> is_number v.
  move=> P Q. inversion Q => //.
    - rewrite -H in P. inversion P.
    - rewrite -H in P. inversion P.
Qed.

Theorem th_8_3_2 t1 ty : has_type t1 ty -> is_value t1 \/ exists t2, evaluate t1 t2.
  elim.
    - left. by constructor.
    - left. by constructor.
    - move=> t0 t2 t3 ty0 P Q R S T U. right. case Q.
      + move=> V. case (th_8_3_1_1 P V) => W; rewrite W.
        * exists t2. constructor.
        * exists t3. constructor.
      + elim=> t4 V. exists (TmIfThenElse t4 t2 t3). by constructor.
    - left. constructor. constructor.
    - move=> t2 P Q. case Q.
      + move/(th_8_3_1_2 P) => R. left. constructor. by constructor.
      + elim=> t3 R. right. exists (TmSucc t3). by constructor.
    - move=> t2 P Q. case Q.
      + move/(th_8_3_1_2 P) => R. case R.
        * right. exists TmZero. constructor.
        * move=> t3 S. right. exists t3. constructor.
      + elim=> t3 R. right. exists (TmPred t3). by constructor.
    - move=> t2 P Q. case Q.
      + move/(th_8_3_1_2 P) => R. case R.
        * right. exists TmTrue. constructor.
        * move=> t3 S. right. exists TmFalse. constructor.
      + elim=> t3 R. right. exists (TmIsZero t3). by constructor.
Qed.
 
Theorem th_8_3_3 t1 t2 ty : has_type t1 ty -> evaluate t1 t2 -> has_type t2 ty.
  move: t2 ty.  elim: t1.
    - move=> t2 ty P Q. inversion Q.
    - move=> t2 ty P Q. inversion Q.
    - move=> t1 P t3 Q t4 R t2 ty S T. inversion T.
      + rewrite -H0 in S. move: S => /le_8_2_2_3 [U [V W]]. by rewrite H3 in V.
      + rewrite -H0 in S. move: S => /le_8_2_2_3 [U [V W]]. by rewrite H3 in W.
      + move: S => /le_8_2_2_3 [U [V W]]. move: (P _ _ U H3) => X. by constructor.
    - move=> t2 ty P Q. inversion Q.
    - move=> t1 P t2 ty Q R. inversion R. move: Q. move/le_8_2_2_5. elim=> S T. move: (P _ _ T H0). rewrite S. apply TySucc.
    - move=> t1 P t2 ty Q R. move: Q => /le_8_2_2_6 [S T]. inversion R.
      + rewrite S. constructor.
      + rewrite -H0 in T. move: T => /le_8_2_2_5 [U V]. by rewrite -H1 S.
      + move: (P _ _ T H0). rewrite S. by constructor.
    - move=> t1 P t2 ty Q R. inversion R.
      + move: Q => /le_8_2_2_7 [S T]. rewrite S. constructor.
      + move: Q => /le_8_2_2_7 [S T]. rewrite S. constructor.
      + move: Q => /le_8_2_2_7 [S T]. move: (P _ _ T H0). rewrite S. by constructor.
Qed.
