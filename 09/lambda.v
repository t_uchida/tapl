From Coq.Strings Require Import String Ascii.
From mathcomp.ssreflect Require Import ssreflect ssrfun ssrbool eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Inductive type : Type :=
  | TyArr of type & type
  | TyBool.

Inductive term : Type :=
  | TmVar of string
  | TmAbs of string & type & term
  | TmApp of term & term
  | TmTrue
  | TmFalse
  | TmIf of term & term & term.

Definition eqstring s t :=
  match string_dec s t with
    | left _ => true
    | right _ => false
  end.

Lemma eqstringP : Equality.axiom eqstring.
  rewrite /eqstring /Equality.axiom => x y. by case (string_dec x y) => P; constructor.
Defined.

Canonical string_eqMixin := EqMixin eqstringP.
Canonical string_eqType := Eval hnf in EqType string string_eqMixin.

Fixpoint eqtype (t u : type) := false.
Lemma eqtypeP : Equality.axiom eqtype.
  admit.
Admitted.

Canonical type_eqMixin := EqMixin eqtypeP.
Canonical type_eqType := Eval hnf in EqType type type_eqMixin.

Fixpoint freevar t :=
  match t with
    | TmVar s => [:: s]
    | TmAbs s _ u => rem s (freevar u)
    | TmApp t1 t2 => freevar t1 ++ freevar t2
    | _ => [::]
  end.

Fixpoint substitute x t u :=
  match u with
    | TmVar y => if x == y then t else u
    | TmAbs y ty t1 => if (x != y) && ~~ (y \in freevar t1) then TmAbs y ty (substitute x t t1) else u
    | TmApp t1 t2 => TmApp (substitute x t t1) (substitute x t t2)
    | _ => t
  end.

Inductive evaluate : term -> term -> Prop :=
  | EApp1 : forall t u v, evaluate t u -> evaluate (TmApp t v) (TmApp u v)
  | EApp2 : forall t u v, evaluate t u -> evaluate (TmApp v t) (TmApp v u)
  | EAppAbs : forall x ty t v, evaluate (TmApp (TmAbs x ty t) v) (substitute x v t).

Definition context := seq (string * type).

Inductive has_type : context -> term -> type -> Prop :=
  | TVar : forall c x t, (x, t) \in c -> has_type c (TmVar x) t
  | TAbs : forall c x t T1 T2, has_type ((x, T1) :: c) t T2 -> has_type c (TmAbs x T1 t) (TyArr T1 T2)
  | TApp : forall c t1 t2 T1 T2, has_type c t1 (TyArr T1 T2) -> has_type c t2 T1 -> has_type c (TmApp t1 t2) T2.

Lemma ex_9_3_1_1 c x R : has_type c (TmVar x) R -> (x, R) \in c.
  move=> P. by inversion P.
Qed.

Lemma ex_9_3_1_2 c x T1 t R : has_type c (TmAbs x T1 t) R -> exists R2, has_type ((x, T1) :: c) t R2 /\ R = TyArr T1 R2.
  move=> P. inversion P. exists T2. by split.
Qed.

Lemma ex_9_3_1_3 c t1 t2 R : has_type c (TmApp t1 t2) R -> exists T, has_type c t1 (TyArr T R) /\ has_type c t2 T.
  move=> P. inversion P. exists T1. by split.
Qed.


(*
Implicit Type s : string.
Implicit Type c : context.

Inductive term : context -> Type :=
  | TmVar : forall c s, s \in c -> term c
  | TmAbs : forall c s, term (s :: c) -> term c
  | TmApp : forall c, term c -> term c -> term c
  | TmTrue : forall c, term c
  | TmFalse : forall c, term c
  | TmIf : forall c, term c -> term c -> term c.

Fixpoint substitute x t u :  :=
  match u with
    | TmVar _ y _ => if x == y then t else u
    | _ => u
  end.

Fixpoint eqterm (t u : term) := false.
Lemma eqtermP : Equality.axiom eqterm.
  admit.
Qed.

Canonical term_eqMixin := EqMixin eqtermP.
Canonical term_eqType := Eval hnf in EqType term term_eqMixin.

Definition context := seq (term * type).

Inductive typeof : context -> term -> type -> Prop :=
  | TTrue : forall c, typeof c TmTrue TyBool
  | TFalse : forall c, typeof c TmFalse TyBool
  | TIf : forall c t1 t2 t3 ty, typeof c t1 TyBool -> typeof c t2 ty -> typeof c t3 ty -> typeof c (TmIf t1 t2 t3) ty
  | TVar : forall c tm ty, (tm, ty) \in c -> typeof c tm ty
  | TAbs : forall c tm1 tm2 ty1 ty2, typeof ((tm1, ty1) :: c) tm2 ty2 -> typeof c (TmAbs ty1 tm1) (TyArr ty1 ty2)
  | TApp : forall c tm1 tm2 ty1 ty2, typeof c tm1 (TyArr ty1 ty2) -> typeof c tm2 ty1 -> typeof c (TmApp tm1 tm2) ty2.

Definition eqstring s t :=
  match string_dec s t with
    | left _ => true
    | right _ => false
  end.

Lemma eqstringP : Equality.axiom eqstring.
  rewrite /eqstring /Equality.axiom => x y. by case (string_dec x y) => P; constructor.
Defined.

Canonical string_eqMixin := EqMixin eqstringP.
Canonical string_eqType := Eval hnf in EqType string string_eqMixin.

Definition context := seq string.

Implicit Type s : string.
Implicit Type c : context.

Inductive term : context -> Type :=
  | TmVar : forall c s, s \in c -> term c
  | TmAbs : forall c s, term (s :: c) -> term c
  | TmApp : forall c, term c -> term c -> term c.

Notation "\ x ~> y" := (@TmAbs _ x y) (at level 110, right associativity).
Notation "x @ y" := (@TmApp _ x y) (at level 90, left associativity).
Notation "% x" := (@TmVar _ x _) (at level 0).

Local Open Scope string.

Inductive uterm : Type :=
  | UTmVar of nat
  | UTmAbs of uterm
  | UTmApp of uterm & uterm.

Fixpoint remove_name c (t : term c) : uterm :=
  match t with
    | TmVar c s _ => UTmVar (index s c)
    | TmAbs c s u => UTmAbs (@remove_name (s :: c) u)
    | TmApp c u v => UTmApp (@remove_name c u) (@remove_name c v)
  end.
*)

