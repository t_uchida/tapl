Require Import ssreflect ssrfun ssrbool eqtype.

Inductive term : Type :=
  | TmTrue : term
  | TmFalse : term
  | TmIfThenElse : term -> term -> term -> term
  | TmZero : term
  | TmSucc : term -> term
  | TmPred : term -> term
  | TmIsZero : term -> term.

Inductive is_number t : Prop :=
  | NZero : t = TmZero -> is_number t
  | NSucc : forall u, t = TmSucc u -> is_number t.

Inductive is_value t : Prop :=
  | VTrue : t = TmTrue -> is_value t
  | VFalse : t = TmFalse -> is_value t
  | VNumber : is_number t -> is_value t.

Inductive evaluate : term -> term -> Prop :=
  | EIfTrue : forall t u, evaluate (TmIfThenElse TmTrue t u) t
  | EIfFalse : forall t u, evaluate (TmIfThenElse TmFalse t u) u
  | EIf : forall t u v w, evaluate t u -> evaluate (TmIfThenElse t v w) (TmIfThenElse u v w)
  | ESucc : forall t u, evaluate t u -> evaluate (TmSucc t) (TmSucc u)
  | EPredZero : evaluate (TmPred TmZero) TmZero
  | EPredSucc : forall t, evaluate (TmPred (TmSucc t)) t
  | EPred : forall t u, evaluate t u -> evaluate (TmPred t) (TmPred u)
  | EIsZeroZero : evaluate (TmIsZero TmZero) TmTrue
  | EIsZeroSucc : forall t, evaluate (TmIsZero (TmSucc t)) TmFalse
  | EIsZero : forall t u, evaluate t u -> evaluate (TmIsZero t) (TmIsZero u).

Theorem th_3_5_14 t u v : evaluate t u -> evaluate t v -> u = v.
  admit.
Qed.

Inductive star : term -> term -> Prop :=
  | S1 : forall t u, evaluate t u -> star t u
  | S2 : forall t, star t t
  | S3 : forall t u v, star t u -> star u v -> star t v.

Inductive bigstep : term -> term -> Prop :=
  | BValue : forall t, is_value t -> bigstep t t
  | BIfTrue : forall t u v w, bigstep t TmTrue -> bigstep u v -> bigstep (TmIfThenElse t u w) v
  | BIfFalse : forall t u v w, bigstep t TmFalse -> bigstep u v -> bigstep (TmIfThenElse t w u) v
  | BSucc : forall t u, bigstep t u -> bigstep (TmSucc t) (TmSucc u)
  | BPredZero : forall t, bigstep t TmZero -> bigstep (TmPred t) TmZero
  | BPredSucc : forall t u, bigstep t (TmSucc u) -> bigstep (TmPred t) u
  | BIsZeroZero : forall t, bigstep t TmZero -> bigstep (TmIsZero t) TmTrue
  | BIsZeroSucc : forall t u, bigstep t (TmSucc u) -> bigstep (TmIsZero t) TmFalse.

Theorem th_3_5_17 t u : star t u <-> bigstep t u.
  admit.
Qed.

