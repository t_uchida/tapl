Require Import ssreflect.ssreflect ssreflect.ssrfun ssreflect.ssrbool ssreflect.eqtype.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive term : Type :=
  | TmTrue : term
  | TmFalse : term
  | TmIfThenElse : term -> term -> term -> term.

Inductive evaluate : term -> term -> Prop :=
  | EIfTrue : forall t u, evaluate (TmIfThenElse TmTrue t u) t
  | EIfFalse : forall t u, evaluate (TmIfThenElse TmFalse t u) u
  | EIf : forall t u v w, evaluate t u -> evaluate (TmIfThenElse t v w) (TmIfThenElse u v w).

Definition is_normal t := forall u, ~ evaluate t u.

Definition dec_normal t :=
  match t with
    | TmTrue => true
    | TmFalse => true
    | TmIfThenElse _ _ _ => false
  end.

Lemma is_normalP t : reflect (is_normal t) (dec_normal t).
  case: t.
    - constructor. move=> t H. inversion H.
    - constructor. move=> t H. inversion H.
    - move=> t u v. constructor. elim: t.
      + move=> P. apply (P u). constructor.
      + move=> P. apply (P v). constructor.
      + move=> t P w Q x R S. move: P. elim. move=> y T. inversion T.
        * rewrite -H0 in S. apply (S (TmIfThenElse w u v)). apply EIf. constructor.
        * rewrite -H0 in S. apply (S (TmIfThenElse x u v)). apply EIf. constructor.
        * apply (S (TmIfThenElse (TmIfThenElse u0 w x) u v)). apply EIf. apply EIf. exact H3.
Qed.

Lemma true_is_normal : is_normal TmTrue.
  move=> t H. inversion H.
Qed.

Lemma false_is_normal : is_normal TmFalse.
  move=> t H. inversion H.
Qed.

Theorem th_3_5_4 t u v : evaluate t u -> evaluate t v -> u = v.
  move=> P. move: v. elim: P => [w x v P|w x v P|t' u' v' w R Q x P]; inversion P => //.
    - contradict H3. apply true_is_normal.
    - contradict H3. apply false_is_normal.
    - rewrite -H0 in R. contradict R. apply true_is_normal.
    - rewrite -H0 in R. contradict R. apply false_is_normal.
    - by rewrite (Q _ H3).
Qed.

Inductive is_value t : Prop :=
  | VTrue : t = TmTrue -> is_value t
  | VFalse : t = TmFalse -> is_value t.

Definition dec_value t :=
  match t with
    | TmTrue => true
    | TmFalse => true
    | TmIfThenElse _ _ _ => false
  end.

Lemma is_valueP t : reflect (is_value t) (dec_value t).
  elim: t.
    - constructor. by apply VTrue.
    - constructor. by apply VFalse.
    - move=> t P u Q v R. constructor. move=> S. by inversion S.
Qed.

Theorem th_3_5_7 t : is_value t -> is_normal t.
  move/is_valueP => P. apply/is_normalP. by case: t P.
Qed.

Theorem th_3_5_8 t : is_normal t -> is_value t.
  move/is_normalP => P. apply/is_valueP. by case: t P.
Qed.

Inductive star : term -> term -> Prop :=
  | S1 : forall t u, evaluate t u -> star t u
  | S2 : forall t, star t t
  | S3 : forall t u v, star t u -> star u v -> star t v.

Lemma star_ind' (P : term -> term -> Prop) :
    (forall t : term, P t t) ->
    (forall t u v : term, evaluate t u -> star u v -> P u v -> P t v) ->
    forall t u : term, star t u -> P t u.
  move=> Q R t u S. move: P Q R. elim: S.
    - move=> v w Q P R S. apply (S _ _ _ Q (S2 _) (R _)).
    - move=> v P Q R. apply Q.
    - move=> v w x P Q R S T U V. pose W := S T U V. apply (V v w x) => //. admit.
Admitted.

Lemma star_normal_refl t u : star t u -> is_normal t -> t = u.
  elim => // [v w P Q|v w x P Q R S T].
    - contradict P. apply Q.
    - pose U := Q T. rewrite -U in S. apply/S/T.
Qed.

Theorem th_3_5_11 t u v : star t u -> star t v -> is_normal u -> is_normal v -> u = v.
  move=> P. move: v. elim/star_ind': P => [w v P Q R|v w x P Q R z S].
    - apply (star_normal_refl P Q).
    - move: w x Q R P. elim/star_ind': S => [w x y P Q R S T|w x y P Q R a b S T U V W].
      + contradict R. apply T.
      + pose X := th_3_5_4 P U. rewrite X in Q. apply (T _ Q V W).
Qed.

Theorem th_3_5_12 t : exists u, is_normal u /\ star t u.
Admitted.
