From Coq.Strings Require Import String Ascii.
From mathcomp.ssreflect Require Import ssreflect ssrfun ssrbool eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition eqstring s t :=
  match string_dec s t with
    | left _ => true
    | right _ => false
  end.

Lemma eqstringP : Equality.axiom eqstring.
  rewrite /eqstring /Equality.axiom => x y. by case (string_dec x y) => P; constructor.
Defined.

Canonical string_eqMixin := EqMixin eqstringP.
Canonical string_eqType := Eval hnf in EqType string string_eqMixin.

Definition context := seq string.

Implicit Type s : string.
Implicit Type c : context.

Inductive term : context -> Type :=
  | TmVar : forall c s, s \in c -> term c
  | TmAbs : forall c s, term (s :: c) -> term c
  | TmApp : forall c, term c -> term c -> term c.

Notation "\ x ~> y" := (@TmAbs _ x y) (at level 110, right associativity).
Notation "x @ y" := (@TmApp _ x y) (at level 90, left associativity).
Notation "% x" := (@TmVar _ x _) (at level 0).

Local Open Scope string.

Program Example term_1 : term nil := \"s" ~> \"z" ~> %"z" @ %"s".

Program Example term_2 : term nil := \"s" ~> \"z" ~> %"s" @ %"s" @ %"z".

Program Example term_plus : term nil := \"x" ~> \"y" ~> \"s" ~> \"z" ~> %"x" @ %"s" @ (%"y" @ %"s" @ %"z").

Program Example term_succ : term nil := \"x" ~> \"s" ~> \"z" ~> %"s" @ (%"x" @ %"s" @ %"z").

Inductive uterm : Type :=
  | UTmVar of nat
  | UTmAbs of uterm
  | UTmApp of uterm & uterm.

Fixpoint remove_name c (t : term c) : uterm :=
  match t with
    | TmVar c s _ => UTmVar (index s c)
    | TmAbs c s u => UTmAbs (@remove_name (s :: c) u)
    | TmApp c u v => UTmApp (@remove_name c u) (@remove_name c v)
  end.

Example uterm_1 := UTmAbs (UTmAbs (UTmApp (UTmVar 0) (UTmVar 1))).

Lemma term_1_eq_uterm_1 : remove_name term_1 = uterm_1.
  by rewrite /uterm_1 /term_1.
Qed.

Example uterm_2 := UTmAbs (UTmAbs (UTmApp (UTmApp (UTmVar 1) (UTmVar 1)) (UTmVar 0))).

Lemma term_2_eq_uterm_2 : remove_name term_2 = uterm_2.
  by rewrite /uterm_1 /term_1.
Qed.

Fixpoint shift (c d : nat) t :=
  match t with
    | UTmVar k => if k < c then UTmVar k else UTmVar (k + d)
    | UTmAbs t1 => UTmAbs (shift c.+1 d t1)
    | UTmApp t1 t2 => UTmApp (shift c d t1) (shift c d t2)
  end.

Eval compute in shift 0 2 (UTmAbs (UTmAbs (UTmApp (UTmVar 1) (UTmApp (UTmVar 0) (UTmVar 2))))).

Fixpoint substitute (j : nat) (s : uterm) t :=
  match t with
    | UTmVar k => if k == j then s else UTmVar k
    | UTmAbs t1 => UTmAbs (substitute j.+1 (shift 0 1 s) t1)
    | UTmApp t1 t2 => UTmApp (substitute j s t1) (substitute j s t2)
  end.